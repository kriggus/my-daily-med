package se.fkstudios.mydailymed.viewmodels.schedule

import android.databinding.BaseObservable
import android.databinding.Bindable
import android.graphics.drawable.Drawable
import se.fkstudios.mydailymed.BR
import se.fkstudios.mydailymed.R
import se.fkstudios.mydailymed.models.Dosage
import se.fkstudios.mydailymed.models.DosageSchedule
import se.fkstudios.mydailymed.models.dateFormat
import se.fkstudios.mydailymed.viewmodels.common.DrawableProvider
import se.fkstudios.mydailymed.viewmodels.common.StringProvider
import java.text.SimpleDateFormat
import java.util.*

class DosageViewModel(
        val dosage: Dosage,
        private val dosageSchedule: DosageSchedule,
        private val stringProvider: StringProvider,
        private val drawableProvider: DrawableProvider,
        private val timeFormat: SimpleDateFormat = dateFormat("HH:mm")
) : BaseObservable() {

    val id: Int
        @Bindable get() = dosage.id

    val name: String
        @Bindable get() = dosage.medicine.name

    val isEnabled: Boolean
        @Bindable get() = dosage.isEnabled

    val isDosed: Boolean
        @Bindable get() = dosage.dosed != null

    val dosedToggleDrawable: Drawable
        @Bindable get() = drawableProvider.get(if (isDosed) R.drawable.ic_clear_black_36dp else R.drawable.ic_check_black_36dp)

    val dosedDisplayString: String
        @Bindable get() = when {
            dosage.dosed == null -> stringProvider.get(R.string.not_dosed)
            else -> "${stringProvider.get(R.string.dosed)} ${timeFormat.format(dosage.dosed)}"
        }

    val plannedDisplayString: String
        @Bindable get() = "${stringProvider.get(R.string.planned)} ${timeFormat.format(dosage.planned)}"

    val backgroundDrawable: Drawable
        @Bindable get() = when {
            isDosed -> drawableProvider.get(R.color.dosed)
            !dosage.isEnabled -> drawableProvider.get(R.color.doseDisabled)
            missedToDose() -> drawableProvider.get(R.color.doseMissed)
            isNextToDose() -> drawableProvider.get(R.color.doseNext)
            else -> drawableProvider.get(R.color.doseLater)
        }

    fun notifyEnabledChanged() {
        notifyPropertyChanged(BR.enabled)
        notifyPropertyChanged(BR.backgroundDrawable)
    }

    fun notifyDosedChanged() {
        notifyPropertyChanged(BR.dosed)
        notifyPropertyChanged(BR.dosedDisplayString)
        notifyPropertyChanged(BR.dosedToggleDrawable)
        notifyPropertyChanged(BR.backgroundDrawable)
    }

    fun notifyPlannedChanged() {
        notifyPropertyChanged(BR.plannedDisplayString)
        notifyPropertyChanged(BR.backgroundDrawable)
    }

    private fun missedToDose(): Boolean = !isDosed && Date() > dosage.planned

    private fun isNextToDose(): Boolean {
        val plannedNext: Date = dosageSchedule.sortedDosages.find { it.isEnabled && it.dosed == null }?.planned ?: return false
        return dosage.isEnabled && dosage.dosed == null && dosage.planned == plannedNext
    }
}