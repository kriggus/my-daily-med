package se.fkstudios.mydailymed.viewmodels.medicine

import android.databinding.BaseObservable
import android.databinding.Bindable
import android.databinding.ObservableArrayList
import android.util.Log
import se.fkstudios.mydailymed.BR
import se.fkstudios.mydailymed.R
import se.fkstudios.mydailymed.models.db.DbManager
import se.fkstudios.mydailymed.models.db.ReadableDbFacade
import se.fkstudios.mydailymed.viewmodels.common.StringProvider

class MedicinesViewModel(
        private val stringProvider: StringProvider,
        private val dbFacade: ReadableDbFacade = DbManager.readableDbFacade)
    : BaseObservable() {

    val medicines: ObservableArrayList<MedicineViewModel> = ObservableArrayList<MedicineViewModel>()

    init {
        update()
    }

    val helpDisplayString: String
        @Bindable get() = when {
            medicines.isEmpty() -> stringProvider.get(R.string.empty_no_medicines)
            else -> ""
        }

    val hasMedicines: Boolean
        @Bindable get() = medicines.isNotEmpty()

    fun update() {
        dbFacade.medicines({ medicines ->
            this.medicines.clear()
            this.medicines.addAll(medicines.map { MedicineViewModel(it, stringProvider) })

            notifyPropertyChanged(BR.helpDisplayString)
            notifyPropertyChanged(BR.hasMedicines)
        }, { error ->
            Log.d("MedicinesViewModel", error.message, error)
        })
    }
}