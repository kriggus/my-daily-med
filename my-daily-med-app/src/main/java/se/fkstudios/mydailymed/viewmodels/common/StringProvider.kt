package se.fkstudios.mydailymed.viewmodels.common

interface StringProvider {
    fun get(id: Int): String
}