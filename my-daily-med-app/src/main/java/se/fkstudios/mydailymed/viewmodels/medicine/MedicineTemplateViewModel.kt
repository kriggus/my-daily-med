package se.fkstudios.mydailymed.viewmodels.medicine

import android.databinding.BaseObservable
import android.databinding.Bindable
import android.util.Log
import se.fkstudios.mydailymed.BR
import se.fkstudios.mydailymed.R
import se.fkstudios.mydailymed.models.MedicineDosageMode
import se.fkstudios.mydailymed.models.MedicineDosageMode.specificDosages
import se.fkstudios.mydailymed.models.db.DbManager
import se.fkstudios.mydailymed.models.db.ReadableDbFacade
import se.fkstudios.mydailymed.viewmodels.common.StringProvider

class MedicineTemplateViewModel(
        private val stringProvider: StringProvider
) : BaseObservable() {

    var id: Int? = null
        private set

    var name: String = ""
        @Bindable get() = field
        set(value) {
            if (field != value) {
                field = value;
                notifyPropertyChanged(BR.name)
                notifyPropertyChanged(BR.valid)
            }
        }

    var dosageMode: MedicineDosageMode = specificDosages
        @Bindable get() = field
        set(value) {
            if (field != value) {
                field = value
                timeTemplates.clear()
                addTimeTemplate()
                notifyPropertyChanged(BR.dosageMode)
            }
        }

    val timeTemplates: MutableList<MedicineTimeTemplateViewModel> = mutableListOf()
        @Bindable get() = field

    val isValid: Boolean
        @Bindable get() = name.isNotEmpty() && timeTemplates.isNotEmpty()

    init {
        addTimeTemplate()
    }

    constructor(
            medicineId: Int,
            stringProvider: StringProvider,
            dbFacade: ReadableDbFacade = DbManager.readableDbFacade
    ) : this(stringProvider) {
        dbFacade.findMedicine(medicineId, { medicine ->
            medicine?.let {
                id = medicine.id
                name = medicine.name
                dosageMode = medicine.dosageMode
                timeTemplates.clear()
                medicine.times.forEach {
                    val timeTemplateViewModel = MedicineTimeTemplateViewModel(it.id, it.hourOfDay, it.minute, getTimeDisplayStringSuffix())
                    timeTemplates.add(timeTemplateViewModel)
                }
                notifyPropertyChanged(BR.timeTemplates)
            }
        }, { error ->
            Log.e("MedicineTmplViewModel", error.message, error)
        })
    }

    fun addTimeTemplate() {
        val timeTemplate = MedicineTimeTemplateViewModel(
                hourOfDay = getNextStartHourOfDay(),
                minute = getNextStartMinute(),
                timeDisplayStringSuffix = getTimeDisplayStringSuffix()
        )
        timeTemplates.add(timeTemplate)
        notifyPropertyChanged(BR.timeTemplates)
    }

    fun removeTimeTemplate(timeTemplate: MedicineTimeTemplateViewModel) {
        timeTemplates.remove(timeTemplate)
        notifyPropertyChanged(BR.timeTemplates)
    }

    private fun hasTimeViewModels(): Boolean = timeTemplates.size > 0

    private fun getTimeDisplayStringSuffix() = stringProvider.get(
            when (dosageMode) {
                MedicineDosageMode.specificDosages -> R.string.every_day
                MedicineDosageMode.relativeDosages -> if (hasTimeViewModels()) R.string.after_previous else R.string.every_day
            })

    private fun getNextStartHourOfDay() = when (dosageMode) {
        MedicineDosageMode.specificDosages -> if (hasTimeViewModels()) (timeTemplates.last().hourOfDay + 2) % 24 else 9
        MedicineDosageMode.relativeDosages -> if (hasTimeViewModels()) 1 else 9
    }

    private fun getNextStartMinute() = when (dosageMode) {
        MedicineDosageMode.specificDosages -> if (hasTimeViewModels()) timeTemplates.last().minute else 0
        MedicineDosageMode.relativeDosages -> 0
    }
}