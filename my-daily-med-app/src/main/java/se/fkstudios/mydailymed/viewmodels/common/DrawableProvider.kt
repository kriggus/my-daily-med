package se.fkstudios.mydailymed.viewmodels.common

import android.graphics.drawable.Drawable

interface DrawableProvider {
    fun get(id: Int): Drawable
}