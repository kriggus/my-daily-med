package se.fkstudios.mydailymed.viewmodels.common

class DelegateCommand(
        private val doExecute: () -> Unit,
        private val doCanExecute: () -> Boolean = { true })
    : ExecutableCommand {

    override fun execute() = if (canExecute()) doExecute() else Unit

    override fun canExecute(): Boolean = doCanExecute()
}