package se.fkstudios.mydailymed.viewmodels.medicine

import android.databinding.BaseObservable
import android.databinding.Bindable
import se.fkstudios.mydailymed.R
import se.fkstudios.mydailymed.models.Medicine
import se.fkstudios.mydailymed.models.MedicineDosageMode.relativeDosages
import se.fkstudios.mydailymed.models.MedicineDosageMode.specificDosages
import se.fkstudios.mydailymed.viewmodels.common.StringProvider

class MedicineViewModel(
        medicine: Medicine,
        private val stringProvider: StringProvider)
    : BaseObservable() {

    val medicine: Medicine = medicine

    val id: Int
        @Bindable get() = medicine.id

    val name: String
        @Bindable get() = medicine.name

    val timesDisplayString: String
        @Bindable get() = "${medicine.times.size} ${timeTypeDisplayString()} ${stringProvider.get(R.string.every_day)}"

    private fun timeTypeDisplayString() = when (medicine.dosageMode) {
        specificDosages -> stringProvider.get(R.string.specific_times)
        relativeDosages -> stringProvider.get(R.string.relative_times)
    }
}