package se.fkstudios.mydailymed.viewmodels.schedule

import android.databinding.BaseObservable
import android.databinding.Bindable
import android.databinding.ObservableArrayList
import se.fkstudios.mydailymed.BR
import se.fkstudios.mydailymed.R
import se.fkstudios.mydailymed.models.DosageSchedule
import se.fkstudios.mydailymed.models.dateFormat
import se.fkstudios.mydailymed.models.db.DbManager
import se.fkstudios.mydailymed.models.db.ReadableDbFacade
import se.fkstudios.mydailymed.models.prefs.SelectedDateSharedPreferences
import se.fkstudios.mydailymed.viewmodels.common.DrawableProvider
import se.fkstudios.mydailymed.viewmodels.common.StringProvider
import java.text.SimpleDateFormat
import java.util.*

class DosageScheduleViewModel(
        var dosageSchedule: DosageSchedule? = null,
        var dosageViewModels: ObservableArrayList<DosageViewModel> = ObservableArrayList<DosageViewModel>(),
        private val stringProvider: StringProvider,
        private val drawableProvider: DrawableProvider,
        private val dateFormatWithWeekday: SimpleDateFormat = dateFormat("yyyy-MM-dd (EEEE)"),
        private val dbFacade: ReadableDbFacade = DbManager.readableDbFacade,
        private val selectedDatePrefs: SelectedDateSharedPreferences)
    : BaseObservable() {

    var day: Date
        get() = selectedDatePrefs.load()
        set(value) {
            selectedDatePrefs.save(value)
        }

    val dayDisplayString: String
        @Bindable get() = dateFormatWithWeekday.format(day)

    val helpDisplayString: String
        @Bindable get() = when {
            dosageSchedule?.dosages?.isEmpty() == true -> stringProvider.get(R.string.empty_no_dosages)
            else -> ""
        }

    val hasDosages: Boolean
        @Bindable get() = dosageSchedule?.dosages?.isNotEmpty() == true

    fun update() {
        dbFacade.findDosageSchedule(day, { dosageSchedule ->
            dosageViewModels.clear()
            dosageViewModels.addAll(dosageSchedule?.sortedDosages
                    ?.map { DosageViewModel(it, dosageSchedule, stringProvider, drawableProvider) }
                    ?: listOf())

            this.dosageSchedule = dosageSchedule

            notifyPropertyChanged(BR.dayDisplayString)
            notifyPropertyChanged(BR.helpDisplayString)
            notifyPropertyChanged(BR.hasDosages)
        })
    }

    fun notifyDosagesEnabledChanged() {
        dosageViewModels.forEach { it.notifyEnabledChanged() }
    }

    fun notifyDosagesIsDosedChanged() {
        dosageViewModels.forEach { it.notifyDosedChanged() }
    }

    fun notifyDosagesPlannedChanged() {
        dosageViewModels.forEach { it.notifyPlannedChanged() }
    }
}