package se.fkstudios.mydailymed.viewmodels.common

interface ExecutableCommand {
    fun execute()
    fun canExecute(): Boolean
}