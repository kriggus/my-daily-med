package se.fkstudios.mydailymed.viewmodels.medicine

import android.databinding.BaseObservable
import android.databinding.Bindable
import se.fkstudios.mydailymed.BR
import se.fkstudios.mydailymed.models.date
import se.fkstudios.mydailymed.models.dateFormat
import java.text.SimpleDateFormat

class MedicineTimeTemplateViewModel(
        id: Int? = null,
        hourOfDay: Int,
        minute: Int,
        private val timeDisplayStringSuffix: String,
        private val timeFormat: SimpleDateFormat = dateFormat("HH:mm")
) : BaseObservable() {

    val id: Int? = id

    var hourOfDay: Int = hourOfDay
        private set

    var minute: Int = minute
        private set

    val timeDisplayString: String
        @Bindable get() {
            return "${timeFormat.format(date(hourOfDay, minute))} $timeDisplayStringSuffix"
        }

    fun setTime(hourOfDay: Int, minute: Int) {
        this.hourOfDay = hourOfDay
        this.minute = minute
        notifyPropertyChanged(BR.timeDisplayString)
    }
}