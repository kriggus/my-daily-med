package se.fkstudios.mydailymed.viewmodels.nextdosage

import android.databinding.BaseObservable
import android.databinding.Bindable
import android.graphics.drawable.Drawable
import com.android.databinding.library.baseAdapters.BR
import se.fkstudios.mydailymed.R
import se.fkstudios.mydailymed.models.*
import se.fkstudios.mydailymed.models.db.DbManager
import se.fkstudios.mydailymed.models.db.ReadableDbFacade
import se.fkstudios.mydailymed.models.prefs.SelectedDateSharedPreferences
import se.fkstudios.mydailymed.viewmodels.common.DrawableProvider
import se.fkstudios.mydailymed.views.toCommaSeparatedDisplayString
import java.text.SimpleDateFormat
import java.util.*

class NextDosageViewModel(
        var schedule: DosageSchedule? = null,
        private val drawableProvider: DrawableProvider,
        private val timeFormatTimeAtNext: SimpleDateFormat = dateFormat("H:mm"),
        private val timeFormatTimeToNext: SimpleDateFormat = dateFormatUtc("H:mm:ss"),
        private val dateFormatWithWeekday: SimpleDateFormat = dateFormat("yyyy-MM-dd (EEEE)"),
        private val dbFacade: ReadableDbFacade = DbManager.readableDbFacade,
        private val selectedDatePrefs: SelectedDateSharedPreferences
) : BaseObservable() {

    var day: Date
        get() = selectedDatePrefs.load()
        set(value) {
            selectedDatePrefs.save(value)
        }

    val dayDisplayString: String
        @Bindable get() = dateFormatWithWeekday.format(day)

    val timeToNextDisplayString: String
        @Bindable get() {
            val nextDosageTime: Date = nextPlanned() ?: return ""
            val now = now()
            val timeDiff = Math.abs(nextDosageTime.time - now.time)
            val timePrefix = if (now > nextDosageTime) "-" else ""
            return "$timePrefix${timeFormatTimeToNext.format(Date(timeDiff))}"
        }

    val timeAtNextDisplayString: String
        @Bindable get() {
            val nextDosageTime: Date = nextPlanned() ?: return ""
            return "at ${timeFormatTimeAtNext.format(nextDosageTime)}"
        }

    val nextMedicinesDisplayString: String
        @Bindable get() = nextDosages
                .map { it.medicine.name }
                .toCommaSeparatedDisplayString()

    val nextDosages: List<Dosage>
        get() = schedule?.dosages
                ?.filter { it.isEnabled && it.dosed == null && it.planned == nextPlanned() }
                ?: listOf()

    val hasDosages: Boolean
        @Bindable get() = nextPlanned() != null

    val backgroundDrawable: Drawable
        @Bindable get() {
            val nextDosageTime: Date = nextPlanned() ?: return drawableProvider.get(R.drawable.shape_circle_dosed_dosages)
            return if (now() > nextDosageTime) {
                drawableProvider.get(R.drawable.shape_circle_missed_dosages)
            } else {
                drawableProvider.get(R.drawable.shape_circle_upcoming_dosages)
            }
        }

    fun update() {
        dbFacade.findDosageSchedule(day, { dosageSchedule ->
            this.schedule = dosageSchedule
            notifyChange()
        })
    }

    fun countDown() {
        notifyPropertyChanged(BR.timeToNextDisplayString)
        notifyPropertyChanged(BR.backgroundDrawable)
    }

    private fun nextPlanned(): Date? = schedule?.sortedDosages
            ?.find { it.isEnabled && it.dosed == null }?.planned

    private fun now(): Date = Date()

}