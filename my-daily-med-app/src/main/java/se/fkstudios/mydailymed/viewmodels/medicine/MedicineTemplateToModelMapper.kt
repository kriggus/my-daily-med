package se.fkstudios.mydailymed.viewmodels.medicine

import se.fkstudios.mydailymed.models.Medicine
import se.fkstudios.mydailymed.models.MedicineTime

class MedicineTemplateToModelMapper() {

    fun toModel(viewModel: MedicineTemplateViewModel): Medicine {
        val times: MutableList<MedicineTime> = viewModel.timeTemplates
                .map { timeViewModel ->
                    toModel(timeViewModel)
                }
                .toMutableList()

        val medicineId = viewModel.id
        return when (medicineId) {
            null -> Medicine(name = viewModel.name, dosageMode = viewModel.dosageMode, times = times)
            else -> Medicine(medicineId, viewModel.name, viewModel.dosageMode, times)
        }
    }

    private fun toModel(viewModel: MedicineTimeTemplateViewModel): MedicineTime = when {
        viewModel.id != null -> MedicineTime(viewModel.id, viewModel.hourOfDay, viewModel.minute)
        else -> MedicineTime(hourOfDay = viewModel.hourOfDay, minute = viewModel.minute)
    }

}