package se.fkstudios.mydailymed.models.prefs

import android.content.Context
import android.content.SharedPreferences
import se.fkstudios.mydailymed.models.minus
import se.fkstudios.mydailymed.models.startOfDay
import se.fkstudios.mydailymed.models.startOfToday
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class SelectedDateSharedPreferences(
        context: Context,
        private val sharedPrefs: SharedPreferences? = context.getSharedPreferences(SHARED_PREFS_FILE_KEY, Context.MODE_PRIVATE),
        private val dateFormat: SimpleDateFormat = SimpleDateFormat("yyyyMMdd", Locale.getDefault()),
        private val dateTimeFormat: SimpleDateFormat = SimpleDateFormat("yyyyMMddHHmmss ", Locale.getDefault())
) {

    fun load(): Date {
        val dateString: String = sharedPrefs?.getString(SELECTED_DATE_KEY, null) ?: ""
        return try {
            dateFormat.parse(dateString)
        } catch (e: ParseException) {
            startOfToday()
        }
    }

    fun save(selected: Date) {
        val dateString: String = dateFormat.format(selected.startOfDay)
        sharedPrefs?.edit()?.putString(SELECTED_DATE_KEY, dateString)?.apply()
    }

    fun lastLastUsed(): Date? {
        val dateString: String = sharedPrefs?.getString(SELECTED_DATE_LAST_USED, null) ?: ""
        return try {
            dateTimeFormat.parse(dateString)
        } catch (e: ParseException) {
            null
        }
    }

    fun saveLastUsedToNow() {
        val dateString: String = dateTimeFormat.format(Date())
        sharedPrefs?.edit()?.putString(SELECTED_DATE_LAST_USED, dateString)?.apply()
    }

    fun used30MinAgo(): Boolean {
        val lastUsed: Date? = lastLastUsed()
        return lastUsed != null && (Date() - lastUsed).time > 1000L * 60L * 30L
    }

    companion object {
        const val SELECTED_DATE_KEY = "se.fkstudios.mydailymed.SELECTED_DATE_KEY"
        const val SELECTED_DATE_LAST_USED = "se.fkstudios.mydailymed.SELECTED_DATE_LAST_USED"
    }
}