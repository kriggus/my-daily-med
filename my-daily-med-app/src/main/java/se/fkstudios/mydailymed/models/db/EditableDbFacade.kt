package se.fkstudios.mydailymed.models.db

import se.fkstudios.mydailymed.models.Dosage
import se.fkstudios.mydailymed.models.DosageSchedule
import se.fkstudios.mydailymed.models.Medicine
import java.util.*

interface EditableDbFacade : ReadableDbFacade {

    fun addMedicine(
            medicine: Medicine,
            successCallback: (() -> Unit)? = null,
            errorCallback: ((error: Throwable) -> Unit)? = null
    )

    fun addOrUpdateMedicine(
            medicine: Medicine,
            successCallback: (() -> Unit)? = null,
            errorCallback: ((error: Throwable) -> Unit)? = null
    )

    fun updateMedicine(
            medicine: Medicine,
            successCallback: (() -> Unit)? = null,
            errorCallback: ((error: Throwable) -> Unit)? = null
    )

    fun removeMedicine(
            medicine: Medicine,
            successCallback: (() -> Unit)? = null,
            errorCallback: ((error: Throwable) -> Unit)? = null
    )

    fun recalculateDosageSchedule(
            day: Date,
            resultCallback: ((dosageSchedule: DosageSchedule) -> Unit),
            errorCallback: ((error: Throwable) -> Unit)? = null
    )

    fun updateDosage(
            dosageSchedule: DosageSchedule,
            dosage: Dosage,
            successCallback: (() -> Unit)? = null,
            errorCallback: ((error: Throwable) -> Unit)? = null
    )

    fun updateDosages(
            dosageSchedule: DosageSchedule,
            dosages: List<Dosage>,
            successCallback: (() -> Unit)? = null,
            errorCallback: ((error: Throwable) -> Unit)? = null
    )
}