package se.fkstudios.mydailymed.models

import java.util.*

data class Medicine(
        val id: Int = -1,
        var name: String,
        var dosageMode: MedicineDosageMode,
        var times: MutableList<MedicineTime>,
        var isEnabled: Boolean = true)

data class Dosage(
        val id: Int = -1,
        val medicine: Medicine,
        var planned: Date,
        var dosed: Date? = null,
        var isEnabled: Boolean = true,
        var notifiedAsUpcoming: Boolean = false,
        var notifiedAsMissed: Boolean = false) {

    constructor(id: Int = -1,
                medicine: Medicine,
                plannedDay: Date,
                plannedHourOfDay: Int,
                plannedMinute: Int,
                dosed: Date? = null,
                enabled: Boolean = true)
            : this(id, medicine, plannedDay.startOfDay, dosed, enabled) {
        planned.addHoursOfDay(plannedHourOfDay)
        planned.addMinutes(plannedMinute)
    }
}

data class DosageSchedule(
        val day: Date,
        val dosages: MutableList<Dosage>) {

    val sortedDosages: List<Dosage>
        get() = dosages.toList().sortedBy { it.planned }
}

data class MedicineTime(
        val id: Int = -1,
        var hourOfDay: Int,
        var minute: Int)

enum class MedicineDosageMode(val value: Int) {
    specificDosages(0), relativeDosages(1)
}
