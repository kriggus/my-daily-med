package se.fkstudios.mydailymed.models.db

import android.content.Context

object DbManager {

    val editableFacade: EditableDbFacade = DbFlowFacade()
    val readableDbFacade: ReadableDbFacade = editableFacade

    fun initialize(context: Context) {
        val dbFlowFacade = editableFacade as DbFlowFacade
        dbFlowFacade.initialize(context)
    }
}