package se.fkstudios.mydailymed.models.db

import android.content.Context
import com.raizlabs.android.dbflow.annotation.*
import com.raizlabs.android.dbflow.annotation.OneToMany
import com.raizlabs.android.dbflow.config.DatabaseConfig
import com.raizlabs.android.dbflow.config.DatabaseDefinition
import com.raizlabs.android.dbflow.config.FlowConfig
import com.raizlabs.android.dbflow.config.FlowManager
import com.raizlabs.android.dbflow.kotlinextensions.*
import com.raizlabs.android.dbflow.structure.BaseModel
import com.raizlabs.android.dbflow.structure.database.DatabaseWrapper
import com.raizlabs.android.dbflow.structure.database.transaction.Transaction
import se.fkstudios.mydailymed.models.*
import se.fkstudios.mydailymed.models.MedicineDosageMode.relativeDosages
import se.fkstudios.mydailymed.models.MedicineDosageMode.specificDosages
import se.fkstudios.mydailymed.models.db.MyDailyMedDatabase.DB_NAME
import java.util.*

class DbFlowFacade : EditableDbFacade {

    private val mapper: DbDomainMapper = DbDomainMapper()
    private lateinit var dbDefinition: DatabaseDefinition

    fun initialize(context: Context) {
        FlowManager.init(FlowConfig.builder(context)
                .addDatabaseConfig(DatabaseConfig.builder(javaClass)
                        .databaseName(DB_NAME)
                        .build()
                )
                .build()
        )

        dbDefinition = FlowManager.getDatabase(MyDailyMedDatabase::class.java)
    }

    override fun addMedicine(medicine: Medicine, successCallback: (() -> Unit)?, errorCallback: ((error: Throwable) -> Unit)?) {
        val dbMedicine = mapper.toDbModel(medicine)

        val transaction: Transaction = dbDefinition.beginTransactionAsync({ dbMedicine.insert() })
                .success({ successCallback?.invoke() })
                .error({ _, error -> errorCallback?.invoke(error) })
                .build()

        transaction.execute()
    }

    override fun addOrUpdateMedicine(medicine: Medicine, successCallback: (() -> Unit)?, errorCallback: ((error: Throwable) -> Unit)?) {
        val dbMedicine = mapper.toDbModel(medicine)

        val deleteDosages = (delete(DbMedicineTime::class)
                where (DbMedicineTime_Table.medicine_id eq medicine.id))

        val transactionJob: (DatabaseWrapper) -> Unit = {
            deleteDosages.execute()
            dbMedicine.save()
        }

        val transaction: Transaction = dbDefinition.beginTransactionAsync(transactionJob)
                .success({ successCallback?.invoke() })
                .error({ _, error -> errorCallback?.invoke(error) })
                .build()

        transaction.execute()
    }

    override fun updateMedicine(medicine: Medicine, successCallback: (() -> Unit)?, errorCallback: ((error: Throwable) -> Unit)?) {
        val dbMedicine = mapper.toDbModel(medicine)

        val deleteDosages = (delete(DbMedicineTime::class)
                where (DbMedicineTime_Table.medicine_id eq medicine.id))

        val transactionJob: (DatabaseWrapper) -> Unit = {
            deleteDosages.execute()
            dbMedicine.update()
        }

        val transaction: Transaction = dbDefinition.beginTransactionAsync(transactionJob)
                .success({ successCallback?.invoke() })
                .error({ _, error -> errorCallback?.invoke(error) })
                .build()

        transaction.execute()
    }

    override fun removeMedicine(medicine: Medicine, successCallback: (() -> Unit)?, errorCallback: ((error: Throwable) -> Unit)?) {
        val dbMedicine = mapper.toDbModel(medicine)

        val transaction: Transaction = dbDefinition.beginTransactionAsync({
            dbMedicine.dosages?.forEach { it.delete() }
            dbMedicine.times?.forEach { it.delete() }
            dbMedicine.delete()
        })
                .success({ successCallback?.invoke() })
                .error({ _, error -> errorCallback?.invoke(error) })
                .build()

        transaction.execute()
    }

    override fun findMedicine(medicineId: Int, resultCallback: (medicine: Medicine?) -> Unit, errorCallback: ((error: Throwable) -> Unit)?) {
        val selectStatement =
                (select from DbMedicine::class
                        where (DbMedicine_Table.id eq medicineId))

        selectStatement.async()
                .queryResultCallback({ _, cursor -> resultCallback.invoke(cursor.toModelClose()?.let { mapper.toDomainModel(it) }) })
                .error({ _, error -> errorCallback?.invoke(error) })
                .execute()
    }

    override fun medicines(resultsCallback: ((medicines: List<Medicine>) -> Unit), errorCallback: ((error: Throwable) -> Unit)?) {
        val selectStatement = (select from DbMedicine::class)

        selectStatement.async()
                .queryListResultCallback({ _, result -> resultsCallback.invoke(result.map { mapper.toDomainModel(it) }) })
                .error({ _, error -> errorCallback?.invoke(error) })
                .execute()
    }

    override fun recalculateDosageSchedule(day: Date, resultCallback: (dosageSchedule: DosageSchedule) -> Unit, errorCallback: ((error: Throwable) -> Unit)?) {
        val selectMedicinesStatement = (select from DbMedicine::class)
        val selectDosageScheduleStatement =
                (select from DbDosageSchedule::class
                        where (DbDosageSchedule_Table.day eq day))

        val medicines: List<Medicine> = selectMedicinesStatement.list.map { mapper.toDomainModel(it) }

        var dbDosageSchedule: DbDosageSchedule? = null

        val transactionJob: (DatabaseWrapper) -> Unit = {
            val existingDbDosageSchedule = selectDosageScheduleStatement.result
            if (existingDbDosageSchedule != null) {
                existingDbDosageSchedule.dosages?.map { it.delete() }
                existingDbDosageSchedule.delete()
            }
            val newDbDosageSchedule = mapper.toDbModel(createDosageSchedule(day, medicines))
            newDbDosageSchedule.insert()
            dbDosageSchedule = newDbDosageSchedule
        }

        val transaction: Transaction = dbDefinition.beginTransactionAsync(transactionJob)
                .success({ resultCallback.invoke(mapper.toDomainModel(dbDosageSchedule!!)) })
                .error({ _, error -> errorCallback?.invoke(error) })
                .build()

        transaction.execute()
    }

    override fun findDosageSchedule(day: Date, resultCallback: (dosageSchedule: DosageSchedule?) -> Unit, errorCallback: ((error: Throwable) -> Unit)?) {
        val selectStatement =
                (select from DbDosageSchedule::class
                        where (DbDosageSchedule_Table.day eq day))

        selectStatement.async()
                .queryResultCallback({ _, cursor -> resultCallback.invoke(cursor.toModelClose()?.let { mapper.toDomainModel(it) }) })
                .error({ _, error -> errorCallback?.invoke(error) })
                .execute()
    }

    override fun updateDosage(dosageSchedule: DosageSchedule, dosage: Dosage, successCallback: (() -> Unit)?, errorCallback: ((error: Throwable) -> Unit)?) {
        val dbDosage: DbDosage = mapper.toDbModel(mapper.toDbModel(dosageSchedule), dosage)

        val transaction: Transaction = dbDefinition.beginTransactionAsync({ dbDosage.update() })
                .success({ successCallback?.invoke() })
                .error({ _, error -> errorCallback?.invoke(error) })
                .build()

        transaction.execute()
    }

    override fun updateDosages(dosageSchedule: DosageSchedule, dosages: List<Dosage>, successCallback: (() -> Unit)?, errorCallback: ((error: Throwable) -> Unit)?) {
        val dbDosages: List<DbDosage> = dosages.map { mapper.toDbModel(mapper.toDbModel(dosageSchedule), it) }

        val transaction: Transaction = dbDefinition.beginTransactionAsync({ dbDosages.forEach { it.update() } })
                .success({ successCallback?.invoke() })
                .error({ _, error -> errorCallback?.invoke(error) })
                .build()

        transaction.execute()
    }

    private fun createDosageSchedule(day: Date, medicines: List<Medicine>): DosageSchedule {
        val dosages = createDosages(medicines, day).toMutableList()
        return DosageSchedule(day = day, dosages = dosages)
    }

    private fun createDosages(medicines: List<Medicine>, day: Date): List<Dosage> = medicines.flatMap { medicine -> createDosages(medicine, day) }

    private fun createDosages(medicine: Medicine, day: Date): List<Dosage> = when (medicine.dosageMode) {
        specificDosages -> medicine.times.map { time ->
            Dosage(medicine = medicine, plannedDay = day, plannedHourOfDay = time.hourOfDay, plannedMinute = time.minute)
        }
        relativeDosages -> medicine.times.applyAccumulative(epochBaseDate, { date, time ->
            date.addHoursOfDayToNewDate(time.hourOfDay).addMinutesToNewDate(time.minute)
        }).map { date ->
            Dosage(medicine = medicine, planned = day + date)
        }
    }

    private fun <E, V> Iterable<E>.applyAccumulative(initial: V, transform: (V, E) -> V): Iterable<V> {
        var accumulated = initial
        val destination = mutableListOf<V>()
        for (element in this) {
            accumulated = transform(accumulated, element)
            destination.add(accumulated)
        }
        return destination
    }
}

private class DbDomainMapper {

    fun toDomainModel(dbMedicine: DbMedicine): Medicine {
        dbMedicine.load()
        return Medicine(
                dbMedicine.id,
                dbMedicine.name,
                toDomainModel(dbMedicine.dosageMode),
                dbMedicine.times?.map { toDomainModel(it) }?.toMutableList() ?: mutableListOf(),
                dbMedicine.isEnabled)
    }

    fun toDomainModel(dbMedicineDosageMode: Int): MedicineDosageMode = when (dbMedicineDosageMode) {
        0 -> specificDosages
        1 -> relativeDosages
        else -> throw UnsupportedOperationException("Value $dbMedicineDosageMode of parameter is unsupported")
    }

    fun toDomainModel(dbMedicineTime: DbMedicineTime): MedicineTime =
            MedicineTime(dbMedicineTime.id, dbMedicineTime.hourOfDay, dbMedicineTime.minute)

    fun toDomainModel(dbDosageSchedule: DbDosageSchedule): DosageSchedule {
        dbDosageSchedule.load()
        return DosageSchedule(
                dbDosageSchedule.day,
                dbDosageSchedule.dosages?.map { toDomainModel(it) }?.toMutableList() ?: mutableListOf())
    }

    fun toDomainModel(dbDosage: DbDosage): Dosage =
            Dosage(dbDosage.id,
                    toDomainModel(dbDosage.medicine!!),
                    dbDosage.planned,
                    dbDosage.dosed,
                    dbDosage.isEnabled,
                    dbDosage.isNotifiedAsUpcoming,
                    dbDosage.isNotifiedAsMissed
            )

    fun toDbModel(domainMedicine: Medicine): DbMedicine {
        val dbMedicine = DbMedicine(domainMedicine.id, domainMedicine.name, toDbModel(domainMedicine.dosageMode), domainMedicine.isEnabled)
        dbMedicine.times = domainMedicine.times.map { toDbModel(dbMedicine, it) }
        return dbMedicine
    }

    fun toDbModel(domainDosageMode: MedicineDosageMode): Int = when (domainDosageMode) {
        specificDosages -> 0
        relativeDosages -> 1
    }

    fun toDbModel(dbMedicine: DbMedicine, domainMedicineTime: MedicineTime): DbMedicineTime =
            DbMedicineTime(domainMedicineTime.id, dbMedicine, domainMedicineTime.hourOfDay, domainMedicineTime.minute)

    fun toDbModel(domainDosageSchedule: DosageSchedule): DbDosageSchedule {
        val dbDosageSchedule = DbDosageSchedule(domainDosageSchedule.day)
        dbDosageSchedule.dosages = domainDosageSchedule.dosages.map { toDbModel(dbDosageSchedule, it) }
        return dbDosageSchedule
    }

    fun toDbModel(dbDosageSchedule: DbDosageSchedule, domainDosage: Dosage): DbDosage = DbDosage(
            domainDosage.id,
            toDbModel(domainDosage.medicine),
            dbDosageSchedule,
            domainDosage.planned,
            domainDosage.dosed,
            domainDosage.isEnabled,
            domainDosage.notifiedAsUpcoming,
            domainDosage.notifiedAsMissed)
}

@Table(database = MyDailyMedDatabase::class)
private data class DbMedicine(

        @PrimaryKey(autoincrement = true)
        var id: Int = 0,

        @Column
        var name: String = "",

        @Column
        var dosageMode: Int = 0,

        @Column
        var isEnabled: Boolean = true

) : BaseModel() {

    @get:OneToMany(methods = arrayOf(OneToMany.Method.ALL))
    var times by oneToMany {
        select from DbMedicineTime::class where (DbMedicineTime_Table.medicine_id.eq(id))
    }

    @get:OneToMany(methods = arrayOf(OneToMany.Method.ALL))
    var dosages by oneToMany {
        select from DbDosage::class where (DbDosage_Table.medicine_id.eq(id))
    }
}

@Table(database = MyDailyMedDatabase::class)
private data class DbMedicineTime(

        @PrimaryKey(autoincrement = true)
        var id: Int = 0,

        @ForeignKey(
                stubbedRelationship = true,
                references = arrayOf(ForeignKeyReference(
                        notNull = NotNull(),
                        columnName = "medicine_id",
                        foreignKeyColumnName = "id")))
        var medicine: DbMedicine? = null,

        @Column
        var hourOfDay: Int = 0,

        @Column
        var minute: Int = 0

) : BaseModel()

@Table(database = MyDailyMedDatabase::class)
private data class DbDosageSchedule(

        @PrimaryKey()
        var day: Date = epochBaseDate

) : BaseModel() {

    @get:OneToMany(methods = arrayOf(OneToMany.Method.ALL))
    var dosages by oneToMany {
        select from DbDosage::class where (DbDosage_Table.dosage_schedule_day.eq(day))
    }
}

@Table(database = MyDailyMedDatabase::class)
private data class DbDosage(

        @PrimaryKey(autoincrement = true)
        var id: Int = 0,

        @ForeignKey(
                stubbedRelationship = true,
                references = arrayOf(ForeignKeyReference(
                        notNull = NotNull(),
                        columnName = "medicine_id",
                        foreignKeyColumnName = "id")))
        var medicine: DbMedicine? = null,

        @ForeignKey(
                stubbedRelationship = true,
                references = arrayOf(ForeignKeyReference(
                        notNull = NotNull(),
                        columnName = "dosage_schedule_day",
                        foreignKeyColumnName = "day")))
        var dosageSchedule: DbDosageSchedule? = null,

        @Column
        var planned: Date = epochBaseDate,

        @Column
        var dosed: Date? = null,

        @Column
        var isEnabled: Boolean = true,

        @Column
        var isNotifiedAsUpcoming: Boolean = false,

        @Column
        var isNotifiedAsMissed: Boolean = false

) : BaseModel()
