package se.fkstudios.mydailymed.models

import java.text.SimpleDateFormat
import java.util.*

val epochBaseDate: Date = Date(0)

val Date.startOfDay: Date
    get() {
        val calendar = calendar()
        calendar.time = this
        calendar.set(Calendar.HOUR_OF_DAY, 0)
        calendar.set(Calendar.MINUTE, 0)
        calendar.set(Calendar.SECOND, 0)
        calendar.set(Calendar.MILLISECOND, 0)
        return calendar.time
    }

val Date.calendarYear: Int
    get() {
        val calendar = calendar()
        calendar.time = this
        return calendar.get(Calendar.YEAR)
    }

val Date.monthOfYear: Int
    get() {
        val calendar = calendar()
        calendar.time = this
        return calendar.get(Calendar.MONTH)
    }

val Date.dayOfMonth: Int
    get() {
        val calendar = calendar()
        calendar.time = this
        return calendar.get(Calendar.DAY_OF_MONTH)
    }

val Date.hourOfDay: Int
    get() {
        val calendar = calendar()
        calendar.time = this
        return calendar.get(Calendar.HOUR_OF_DAY)
    }

val Date.minute: Int
    get() {
        val calendar = calendar()
        calendar.time = this
        return calendar.get(Calendar.MINUTE)
    }

fun timeZoneUtc(): TimeZone = TimeZone.getTimeZone("UTC+00:00")

fun calendarUtc(): Calendar = Calendar.getInstance(timeZoneUtc())

fun calendar(): Calendar = Calendar.getInstance()

fun date(year: Int, month: Int, dayOfMonth: Int, hourOfDay: Int, minute: Int): Date {
    val calendar = calendar()
    calendar.set(Calendar.YEAR, year)
    calendar.set(Calendar.MONTH, month)
    calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
    calendar.set(Calendar.HOUR_OF_DAY, hourOfDay)
    calendar.set(Calendar.MINUTE, minute)
    calendar.set(Calendar.SECOND, 0)
    calendar.set(Calendar.MILLISECOND, 0)
    return calendar.time
}

fun date(hourOfDay: Int, minute: Int): Date {
    val calendar = calendar()
    calendar.set(Calendar.HOUR_OF_DAY, hourOfDay)
    calendar.set(Calendar.MINUTE, minute)
    calendar.set(Calendar.SECOND, 0)
    calendar.set(Calendar.MILLISECOND, 0)
    return calendar.time
}

fun date(year: Int, month: Int, dayOfMonth: Int): Date {
    val calendar = calendar()
    calendar.set(Calendar.YEAR, year)
    calendar.set(Calendar.MONTH, month)
    calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
    calendar.set(Calendar.HOUR_OF_DAY, 0)
    calendar.set(Calendar.MINUTE, 0)
    calendar.set(Calendar.SECOND, 0)
    calendar.set(Calendar.MILLISECOND, 0)
    return calendar.time
}

fun startOfToday(): Date = Date().startOfDay

fun currentCalendarYear(): Int = calendar().get(Calendar.YEAR)

fun currentMonthOfYear(): Int = calendar().get(Calendar.MONTH)

fun currentDayOfMonth(): Int = calendar().get(Calendar.DAY_OF_MONTH)

fun currentHourOfDay(): Int = calendar().get(Calendar.HOUR_OF_DAY)

fun currentMinuteOfHour(): Int = calendar().get(Calendar.MINUTE)

fun dateFormatUtc(template: String): SimpleDateFormat {
    val dateFormat = SimpleDateFormat(template, Locale.US)
    dateFormat.timeZone = timeZoneUtc()
    return dateFormat
}

fun dateFormat(template: String): SimpleDateFormat = SimpleDateFormat(template, Locale.US)

fun Date.addHoursOfDay(hours: Int): Date = addMinutes(hours * 60)

fun Date.addMinutes(minutes: Int): Date = addSeconds(minutes * 60)

fun Date.addSeconds(seconds: Int): Date = addMillis(seconds * 1000L)

fun Date.addMillis(millis: Long): Date {
    time += millis
    return this
}

fun Date.addHoursOfDayToNewDate(hours: Int): Date = addMinutesToNewDate(hours * 60)

fun Date.addMinutesToNewDate(minutes: Int): Date = addSecondsToNewDate(minutes * 60)

fun Date.addSecondsToNewDate(seconds: Int): Date = addMillisToNewDate(seconds * 1000L)

fun Date.addMillisToNewDate(millis: Long): Date = Date(this.time + millis)

operator fun Date.plus(other: Date) = Date(this.time + other.time)

operator fun Date.minus(other: Date) = Date(this.time - other.time)