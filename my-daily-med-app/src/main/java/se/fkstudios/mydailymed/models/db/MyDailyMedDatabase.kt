package se.fkstudios.mydailymed.models.db

import com.raizlabs.android.dbflow.annotation.Database

@Database(version = MyDailyMedDatabase.DB_VERSION, foreignKeyConstraintsEnforced = true)
object MyDailyMedDatabase {
    const val DB_VERSION = 1
    const val DB_NAME = "MyDailyMedDatabase"
}