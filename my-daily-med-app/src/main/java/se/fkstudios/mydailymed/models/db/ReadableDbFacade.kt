package se.fkstudios.mydailymed.models.db

import se.fkstudios.mydailymed.models.DosageSchedule
import se.fkstudios.mydailymed.models.Medicine
import java.util.*

interface ReadableDbFacade {

    fun medicines(
            resultsCallback: ((medicines: List<Medicine>) -> Unit),
            errorCallback: ((error: Throwable) -> Unit)? = null
    )

    fun findMedicine(
            medicineId: Int,
            resultCallback: ((medicine: Medicine?) -> Unit),
            errorCallback: ((error: Throwable) -> Unit)? = null
    )

    fun findDosageSchedule(
            day: Date,
            resultCallback: ((dosageSchedule: DosageSchedule?) -> Unit),
            errorCallback: ((error: Throwable) -> Unit)? = null
    )
}