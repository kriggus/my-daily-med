package se.fkstudios.mydailymed.views.nextdosage

import android.app.FragmentManager
import android.content.Context
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import se.fkstudios.mydailymed.R
import se.fkstudios.mydailymed.databinding.FragmentNextDosageBinding
import se.fkstudios.mydailymed.models.*
import se.fkstudios.mydailymed.models.db.DbManager
import se.fkstudios.mydailymed.models.prefs.SelectedDateSharedPreferences
import se.fkstudios.mydailymed.viewmodels.common.DelegateCommand
import se.fkstudios.mydailymed.viewmodels.nextdosage.NextDosageViewModel
import se.fkstudios.mydailymed.views.common.ResourceDrawableProvider
import se.fkstudios.mydailymed.views.common.SelectableFragment
import se.fkstudios.mydailymed.views.time.DatePickerFragment
import java.util.*
import kotlin.concurrent.timerTask

class NextDosageFragment : SelectableFragment() {

    private var viewModel: NextDosageViewModel? = null
    private var countDownTimer: Timer? = null
    private var selectedDatePrefs: SelectedDateSharedPreferences? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val selectedDatePrefs = SelectedDateSharedPreferences(requireContext())
        this.selectedDatePrefs = selectedDatePrefs

        val viewModel = NextDosageViewModel(
                drawableProvider = ResourceDrawableProvider(requireContext()),
                selectedDatePrefs = selectedDatePrefs
        )
        this.viewModel = viewModel

        val binding = DataBindingUtil.inflate<FragmentNextDosageBinding>(inflater, R.layout.fragment_next_dosage, container, false)
        binding.viewModel = viewModel
        binding.setDosedCommand = DelegateCommand({
            val dosageSchedule: DosageSchedule = viewModel.schedule ?: return@DelegateCommand
            val context: Context = context ?: return@DelegateCommand

            if (viewModel.nextDosages.isEmpty()) return@DelegateCommand

            AlertDialog.Builder(context)
                    .setTitle(getString(R.string.set_dosed))
                    .setMessage(getString(R.string.sure_you_want_to_dose_upcoming_medicines))
                    .setPositiveButton(R.string.yes, { _, _ ->
                        stopCountDown()
                        val now = Date()
                        val dosages: List<Dosage> = viewModel.nextDosages
                        dosages.forEach {
                            it.dosed = now
                            it.notifiedAsUpcoming = false
                            it.notifiedAsMissed = false
                        }
                        DbManager.editableFacade.updateDosages(dosageSchedule, dosages, {
                            onSelected()
                            startCountDown()
                        }, { _ ->
                            startCountDown()
                        })
                    })
                    .setNegativeButton(R.string.no, null)
                    .show()
        })
        binding.setDayCommand = DelegateCommand({
            val fragmentManager: FragmentManager = activity?.fragmentManager ?: return@DelegateCommand

            val dialog = DatePickerFragment()
            dialog.year = viewModel.day.calendarYear
            dialog.month = viewModel.day.monthOfYear
            dialog.dayOfMonth = viewModel.day.dayOfMonth
            dialog.listener = { year, month, dayOfMonth ->
                stopCountDown()
                viewModel.day = date(year, month, dayOfMonth)
                viewModel.update()
                startCountDown()
            }
            dialog.show(fragmentManager, null)
        })

        return binding.root
    }

    override fun onResume() {
        super.onResume()

        if (selectedDatePrefs?.used30MinAgo() == true) {
            selectedDatePrefs?.save(startOfToday())
        }

        viewModel?.update()
        startCountDown()
    }

    override fun onPause() {
        stopCountDown()
        selectedDatePrefs?.saveLastUsedToNow()
        super.onPause()
    }

    override fun onSelected() {
        viewModel?.update()
        startCountDown()
    }

    override fun onUnselected() {
        stopCountDown()
    }

    private fun startCountDown() {
        stopCountDown()
        countDownTimer = Timer()
        countDownTimer?.schedule(timerTask {
            viewModel?.countDown()
        }, 0L, 1000L)
    }

    private fun stopCountDown() {
        countDownTimer?.cancel()
        countDownTimer = null
    }
}