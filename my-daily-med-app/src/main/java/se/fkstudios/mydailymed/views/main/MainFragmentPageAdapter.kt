package se.fkstudios.mydailymed.views.main

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import se.fkstudios.mydailymed.views.medicine.MedicinesFragment
import se.fkstudios.mydailymed.views.nextdosage.NextDosageFragment
import se.fkstudios.mydailymed.views.schedule.DosageScheduleFragment

class MainFragmentPageAdapter(fm: FragmentManager?) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment = when (position) {
        0 -> NextDosageFragment()
        1 -> DosageScheduleFragment()
        2 -> MedicinesFragment()
        else -> throw UnsupportedOperationException("Unsupported Position")
    }

    override fun getCount(): Int = 3
}