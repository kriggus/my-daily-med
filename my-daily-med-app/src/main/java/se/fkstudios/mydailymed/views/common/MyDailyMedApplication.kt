package se.fkstudios.mydailymed.views.common

import android.app.Application
import android.content.Intent
import com.crashlytics.android.Crashlytics
import io.fabric.sdk.android.Fabric
import se.fkstudios.mydailymed.models.db.DbManager
import se.fkstudios.mydailymed.views.notifications.NotificationService
import se.fkstudios.mydailymed.views.schedule.GenerateScheduleService
import se.fkstudios.mydailymed.views.startServiceInForegroundIfOreo

class MyDailyMedApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        Fabric.with(applicationContext, Crashlytics())

        DbManager.initialize(applicationContext)

        startServiceInForegroundIfOreo(Intent(applicationContext, NotificationService::class.java))
        startServiceInForegroundIfOreo(Intent(applicationContext, GenerateScheduleService::class.java))
    }
}