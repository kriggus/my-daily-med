package se.fkstudios.mydailymed.views.medicine

import android.databinding.DataBindingUtil
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import se.fkstudios.mydailymed.R
import se.fkstudios.mydailymed.databinding.ListItemMedicineTimeBinding
import se.fkstudios.mydailymed.viewmodels.common.DelegateCommand
import se.fkstudios.mydailymed.viewmodels.medicine.MedicineTemplateViewModel
import se.fkstudios.mydailymed.views.time.TimePickerFragment

class MedicineTimeListViewAdapter(
        private val activity: AppCompatActivity,
        private val viewModel: MedicineTemplateViewModel
) : BaseAdapter() {

    override fun getView(position: Int, view: View?, parent: ViewGroup?): View {
        val medicineTime = viewModel.timeTemplates[position]

        val binding: ListItemMedicineTimeBinding = getOrCreateBinding(view, parent)
        binding.viewModel = medicineTime
        binding.setTimeCommand = DelegateCommand({
            val timePickerFragment = TimePickerFragment()
            timePickerFragment.hour = medicineTime.hourOfDay
            timePickerFragment.minute = medicineTime.minute
            timePickerFragment.listener = { hourOfDay, minute -> medicineTime.setTime(hourOfDay, minute) }
            timePickerFragment.show(activity.fragmentManager, null)
        })
        binding.removeCommand = DelegateCommand({
            AlertDialog.Builder(activity)
                    .setTitle(R.string.delete)
                    .setMessage(R.string.sure_you_want_to_delete)
                    .setPositiveButton(R.string.yes, { _, _ -> viewModel.removeTimeTemplate(medicineTime) })
                    .setNegativeButton(R.string.no, null).show()
        }, {
            position != 0
        })

        return binding.root
    }

    private fun getOrCreateBinding(view: View?, parent: ViewGroup?): ListItemMedicineTimeBinding =
            if (view != null)
                DataBindingUtil.getBinding<ListItemMedicineTimeBinding>(view)
            else
                DataBindingUtil.inflate<ListItemMedicineTimeBinding>(LayoutInflater.from(activity), R.layout.list_item_medicine_time, parent, false)

    override fun getItem(position: Int): Any = viewModel.timeTemplates[position]

    override fun getItemId(position: Int): Long = viewModel.timeTemplates[position].id?.toLong() ?: -1

    override fun getCount(): Int = viewModel.timeTemplates.size
}