package se.fkstudios.mydailymed.views.common

import android.content.Context
import se.fkstudios.mydailymed.viewmodels.common.StringProvider

class ResourceStringProvider(private val context: Context) : StringProvider {
    override fun get(id: Int): String = context.getString(id)
}