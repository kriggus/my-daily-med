package se.fkstudios.mydailymed.views.schedule

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_dosage_schedule.*
import se.fkstudios.mydailymed.R
import se.fkstudios.mydailymed.databinding.FragmentDosageScheduleBinding
import se.fkstudios.mydailymed.models.*
import se.fkstudios.mydailymed.models.db.DbManager
import se.fkstudios.mydailymed.models.prefs.SelectedDateSharedPreferences
import se.fkstudios.mydailymed.viewmodels.common.DelegateCommand
import se.fkstudios.mydailymed.viewmodels.schedule.DosageScheduleViewModel
import se.fkstudios.mydailymed.views.common.ResourceDrawableProvider
import se.fkstudios.mydailymed.views.common.ResourceStringProvider
import se.fkstudios.mydailymed.views.common.SelectableFragment
import se.fkstudios.mydailymed.views.time.DatePickerFragment
import se.fkstudios.mydailymed.views.time.TimePickerFragment

class DosageScheduleFragment : SelectableFragment() {

    private var viewModel: DosageScheduleViewModel? = null
    private val dbFacade = DbManager.editableFacade
    private var selectedDatePrefs: SelectedDateSharedPreferences? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val selectedDatePrefs = SelectedDateSharedPreferences(requireContext())
        this.selectedDatePrefs = selectedDatePrefs

        val viewModel = DosageScheduleViewModel(
                stringProvider = ResourceStringProvider(requireContext()),
                drawableProvider = ResourceDrawableProvider(requireContext()),
                selectedDatePrefs = selectedDatePrefs
        )
        this.viewModel = viewModel

        val binding = DataBindingUtil.inflate<FragmentDosageScheduleBinding>(inflater, R.layout.fragment_dosage_schedule, container, false)
        binding.viewModel = viewModel
        binding.setDayCommand = DelegateCommand({
            val activity = activity ?: return@DelegateCommand

            val dialog = DatePickerFragment()
            dialog.year = viewModel.day.calendarYear
            dialog.month = viewModel.day.monthOfYear
            dialog.dayOfMonth = viewModel.day.dayOfMonth
            dialog.listener = { year, month, dayOfMonth ->
                viewModel.day = date(year, month, dayOfMonth)
                viewModel.update()
            }
            dialog.show(activity.fragmentManager, null)
        })
        binding.moveDosagesEarlierCommand = DelegateCommand({
            val activity = activity ?: return@DelegateCommand
            val dosageSchedule = viewModel.dosageSchedule ?: return@DelegateCommand

            val fragment = TimePickerFragment()
            fragment.minute = 0
            fragment.hour = 1
            fragment.listener = { hours, minutes ->
                viewModel.dosageViewModels.forEach { it.dosage.planned.addHoursOfDay(-hours).addMinutes(-minutes) }
                dbFacade.updateDosages(dosageSchedule, dosageSchedule.dosages, { viewModel.update() })
            }
            fragment.show(activity.fragmentManager, null)
        })
        binding.moveDosagesLaterCommand = DelegateCommand({
            val activity = activity ?: return@DelegateCommand
            val dosageSchedule = viewModel.dosageSchedule ?: return@DelegateCommand

            val fragment = TimePickerFragment()
            fragment.minute = 0
            fragment.hour = 1
            fragment.listener = { hours, minutes ->
                viewModel.dosageViewModels.forEach { it.dosage.planned.addHoursOfDay(hours).addMinutes(minutes) }
                dbFacade.updateDosages(dosageSchedule, dosageSchedule.dosages, { viewModel.update() })
            }
            fragment.show(activity.fragmentManager, null)
        })
        binding.recreateScheduleCommand = DelegateCommand({
            val dosageSchedule = viewModel.dosageSchedule
            if (dosageSchedule == null || dosageSchedule.dosages.isEmpty()) {
                dbFacade.recalculateDosageSchedule(viewModel.day, { viewModel.update() })
            } else {
                context?.let {
                    AlertDialog.Builder(it)
                            .setTitle(R.string.recreate)
                            .setMessage("Are you sure you want to recreate schedule for ${viewModel.dayDisplayString}?")
                            .setPositiveButton(R.string.yes, { _, _ -> dbFacade.recalculateDosageSchedule(viewModel.day, { viewModel.update() }) })
                            .setNegativeButton(R.string.no, null).show()
                }
            }
        })

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel?.let { viewModel ->
            listViewDosageSchedule.adapter = DosageListViewAdapter(requireActivity(), viewModel)
            listViewDosageSchedule.dividerHeight = 0
        }
    }

    override fun onResume() {
        super.onResume()

        if (selectedDatePrefs?.used30MinAgo() == true) {
            selectedDatePrefs?.save(startOfToday())
        }

        viewModel?.update()
    }

    override fun onPause() {
        selectedDatePrefs?.saveLastUsedToNow()
        super.onPause()
    }

    override fun onSelected() {
        viewModel?.update()
    }

    override fun onUnselected() = Unit
}