package se.fkstudios.mydailymed.views.common

import android.databinding.BaseObservable
import android.databinding.ObservableArrayList
import android.databinding.ObservableList
import android.widget.BaseAdapter

class NotifyAdapterOnListChangedCallback<T>(
        private val adapter: BaseAdapter)
    : ObservableList.OnListChangedCallback<ObservableArrayList<T>>() where T : BaseObservable {

    override fun onItemRangeInserted(p0: ObservableArrayList<T>?, p1: Int, p2: Int) = adapter.notifyDataSetChanged()
    override fun onItemRangeMoved(p0: ObservableArrayList<T>?, p1: Int, p2: Int, p3: Int) = adapter.notifyDataSetChanged()
    override fun onChanged(p0: ObservableArrayList<T>?) = adapter.notifyDataSetChanged()
    override fun onItemRangeRemoved(p0: ObservableArrayList<T>?, p1: Int, p2: Int) = adapter.notifyDataSetChanged()
    override fun onItemRangeChanged(p0: ObservableArrayList<T>?, p1: Int, p2: Int) = adapter.notifyDataSetChanged()
}