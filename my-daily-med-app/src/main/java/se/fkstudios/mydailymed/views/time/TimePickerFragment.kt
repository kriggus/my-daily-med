package se.fkstudios.mydailymed.views.time

import android.app.Dialog
import android.app.DialogFragment
import android.app.TimePickerDialog
import android.os.Bundle
import android.widget.TimePicker
import se.fkstudios.mydailymed.R
import se.fkstudios.mydailymed.models.currentHourOfDay
import se.fkstudios.mydailymed.models.currentMinuteOfHour

class TimePickerFragment : DialogFragment(), TimePickerDialog.OnTimeSetListener {

    var listener: ((hourOfDay: Int, minute: Int) -> Unit)? = null
    var hour: Int? = null
    var minute: Int? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog = TimePickerDialog(activity,
            R.style.DateAndTimeDialogTheme,
            this,
            hour ?: currentHourOfDay(),
            minute ?: currentMinuteOfHour(),
            true)

    override fun onTimeSet(view: TimePicker, hourOfDay: Int, minute: Int) {
        listener?.invoke(hourOfDay, minute)
    }
}