package se.fkstudios.mydailymed.views.medicine

import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_medicines.*
import se.fkstudios.mydailymed.R
import se.fkstudios.mydailymed.databinding.FragmentMedicinesBinding
import se.fkstudios.mydailymed.models.Medicine
import se.fkstudios.mydailymed.models.MedicineDosageMode.relativeDosages
import se.fkstudios.mydailymed.models.MedicineDosageMode.specificDosages
import se.fkstudios.mydailymed.models.MedicineTime
import se.fkstudios.mydailymed.models.db.DbManager
import se.fkstudios.mydailymed.viewmodels.common.DelegateCommand
import se.fkstudios.mydailymed.viewmodels.medicine.MedicinesViewModel
import se.fkstudios.mydailymed.views.common.ResourceStringProvider
import se.fkstudios.mydailymed.views.common.SelectableFragment
import se.fkstudios.mydailymed.views.doDelayed

class MedicinesFragment : SelectableFragment() {

    private var viewModel: MedicinesViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val viewModel = MedicinesViewModel(ResourceStringProvider(requireContext()))
        this.viewModel = viewModel

        val binding = DataBindingUtil.inflate<FragmentMedicinesBinding>(inflater, R.layout.fragment_medicines, container, false)
        binding.viewModel = viewModel
        binding.addMedicineCommand = DelegateCommand({
            startActivity(Intent(context, AddOrEditMedicineActivity::class.java))
        })
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val footerView: View = layoutInflater.inflate(R.layout.layout_empty_footer, listViewMedicines, false)
        listViewMedicines.addFooterView(footerView, null, false)
        listViewMedicines.dividerHeight = 0
        viewModel?.let { viewModel ->
            listViewMedicines.adapter = MedicineListViewAdapter(requireContext(), viewModel)
        }
    }

    override fun onResume() {
        super.onResume()
        viewModel?.update()
        doDelayed(333) {
            floatingActionButton.show()
        }
    }

    override fun onPause() {
        floatingActionButton.hide()
        super.onPause()
    }

    override fun onSelected() {
        viewModel?.update()
        doDelayed(333) {
            floatingActionButton.show()
        }
    }

    override fun onUnselected() {
        floatingActionButton.hide()
    }
}