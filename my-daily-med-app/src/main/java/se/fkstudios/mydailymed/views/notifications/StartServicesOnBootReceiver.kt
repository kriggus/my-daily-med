package se.fkstudios.mydailymed.views.notifications

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.Intent.ACTION_BOOT_COMPLETED
import se.fkstudios.mydailymed.views.schedule.GenerateScheduleService
import se.fkstudios.mydailymed.views.startServiceInForegroundIfOreo

class StartServicesOnBootReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        if (intent.action == ACTION_BOOT_COMPLETED) {
            val notificationServiceIntent = Intent(context, NotificationService::class.java)
            context.startServiceInForegroundIfOreo(notificationServiceIntent)

            val generateScheduleIntent = Intent(context, GenerateScheduleService::class.java)
            context.startServiceInForegroundIfOreo(generateScheduleIntent)
        }
    }
}