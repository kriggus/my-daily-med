package se.fkstudios.mydailymed.views.common

import android.support.v4.app.Fragment

abstract class SelectableFragment : Fragment() {
    abstract fun onSelected()
    abstract fun onUnselected()
}