package se.fkstudios.mydailymed.views

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Handler
import android.support.annotation.RequiresApi
import android.support.v4.app.NotificationCompat
import se.fkstudios.mydailymed.R
import java.util.*

fun Context.startServiceInForegroundIfOreo(intent: Intent) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        startForegroundService(intent)
    } else {
        startService(intent)
    }
}

fun Service.startForegroundIfOreo(id: Int) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        val channelId = createNotificationChannel()
        val notificationBuilder = NotificationCompat.Builder(this, channelId)
        val notification = notificationBuilder.setOngoing(true)
                .setPriority(NotificationManager.IMPORTANCE_MAX)
                .setCategory(Notification.CATEGORY_SERVICE)
                .build()
        startForeground(id, notification)
    }
}

@RequiresApi(Build.VERSION_CODES.O)
private fun Service.createNotificationChannel(): String {
    val channel = NotificationChannel(
            packageName,
            getString(R.string.app_name),
            NotificationManager.IMPORTANCE_HIGH
    )
    channel.importance = NotificationManager.IMPORTANCE_HIGH
    val service = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
    service.createNotificationChannel(channel)
    return packageName
}

fun doDelayed(millis: Long, task: () -> Unit) {
    Handler().postDelayed({ task.invoke() }, millis)
}

fun <E> List<E>.toCommaSeparatedDisplayString(): String =
        foldIndexed("") { i, acc, elem ->
            when (i) {
                0 -> elem.toString()
                lastIndex -> acc + " and ${elem.toString()}"
                else -> acc + ", ${elem.toString()}"
            }
        }


fun <E> List<E>.unique(): List<E> = HashSet<E>(this).toList()