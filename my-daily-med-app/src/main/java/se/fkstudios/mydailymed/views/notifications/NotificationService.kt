package se.fkstudios.mydailymed.views.notifications

import android.app.Service
import android.content.Intent
import android.os.IBinder
import se.fkstudios.mydailymed.models.Dosage
import se.fkstudios.mydailymed.models.DosageSchedule
import se.fkstudios.mydailymed.models.addHoursOfDay
import se.fkstudios.mydailymed.models.db.DbManager
import se.fkstudios.mydailymed.models.startOfDay
import se.fkstudios.mydailymed.views.startForegroundIfOreo
import java.util.*
import kotlin.concurrent.timerTask

class NotificationService : Service() {

    private var notifyTimer: Timer? = null

    override fun onCreate() {
        super.onCreate()

        if (notifyTimer == null) {
            notifyTimer = Timer()
            notifyTimer?.scheduleAtFixedRate(timerTask {
                val day = Date().addHoursOfDay(-3).startOfDay
                DbManager.readableDbFacade.findDosageSchedule(day, { schedule ->
                    schedule?.let {
                        val notificationManager = NotificationManager(applicationContext)

                        val missedDosages: List<Dosage> = missedDosages(schedule)
                        val missedDosagesHasNotNotified: Boolean = missedDosages.any { !it.notifiedAsMissed }
                        if (missedDosages.isNotEmpty() && missedDosagesHasNotNotified) {
                            notificationManager.notifyDosagesMissed(schedule, missedDosages, missedMarginMillis(missedDosages))
                        }

                        val inNextMillis: Long = 5 * 60 * 1000
                        val upcomingIn5MinDosages: List<Dosage> = upcomingDosages(schedule, inNextMillis)
                        val upcomingIn5MinHasNotNotified: Boolean = upcomingIn5MinDosages.any { !it.notifiedAsUpcoming }
                        if (upcomingIn5MinDosages.isNotEmpty() && upcomingIn5MinHasNotNotified) {
                            notificationManager.notifyDosagesIn5Minutes(schedule, upcomingIn5MinDosages)
                        }
                    }
                })
            }, 10 * 1000, 60 * 1000)
        }

        startForegroundIfOreo(NOTIFICATION_ID_FOREGROUND_SERVICES)
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        super.onStartCommand(intent, flags, startId)
        return START_STICKY
    }

    override fun onBind(p0: Intent?): IBinder = throw UnsupportedOperationException("Service doesn't support binding")

    override fun onDestroy() {
        super.onDestroy()
        notifyTimer?.cancel()
        notifyTimer = null
    }

    private fun upcomingDosages(schedule: DosageSchedule, inNextMillis: Long): List<Dosage> = schedule.sortedDosages.filter {
        val diffMillis = it.planned.time - currentTime().time
        it.isEnabled && it.dosed == null && (diffMillis in 0..inNextMillis)
    }

    private fun missedDosages(schedule: DosageSchedule): List<Dosage> = schedule.sortedDosages.filter {
        val diffMillis = it.planned.time - currentTime().time
        it.isEnabled && it.dosed == null && (diffMillis < 0)
    }

    private fun missedMarginMillis(missedDosages: List<Dosage>): Long = missedDosages.sortedBy { it.planned }.first().planned.time

    private fun currentTime(): Date = Date()
}