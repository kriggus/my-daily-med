package se.fkstudios.mydailymed.views.schedule

import android.app.Service
import android.content.Intent
import android.os.IBinder
import se.fkstudios.mydailymed.models.db.DbManager
import se.fkstudios.mydailymed.models.startOfToday
import se.fkstudios.mydailymed.views.notifications.NOTIFICATION_ID_FOREGROUND_SERVICES
import se.fkstudios.mydailymed.views.startForegroundIfOreo
import java.util.*
import kotlin.concurrent.timerTask

class GenerateScheduleService : Service() {

    private var generateScheduleTimer: Timer? = null

    override fun onCreate() {
        super.onCreate()

        if (generateScheduleTimer == null) {
            generateScheduleTimer = Timer()
            generateScheduleTimer?.scheduleAtFixedRate(timerTask {
                generateSchedule()
            }, 10 * 1000, 10 * 60 * 1000)
        }

        startForegroundIfOreo(NOTIFICATION_ID_FOREGROUND_SERVICES)
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        super.onStartCommand(intent, flags, startId)
        return START_STICKY
    }

    override fun onBind(p0: Intent?): IBinder = throw UnsupportedOperationException("Service doesn't support binding")

    override fun onDestroy() {
        super.onDestroy()
        generateScheduleTimer?.cancel()
        generateScheduleTimer = null
    }

    private fun generateSchedule() {
        val today: Date = startOfToday()
        val dbFacade = DbManager.editableFacade
        dbFacade.medicines({ medicines ->
            if (medicines.isNotEmpty()) {
                dbFacade.findDosageSchedule(today, { dosageSchedule ->
                    if (dosageSchedule == null || dosageSchedule.dosages.isEmpty()) {
                        dbFacade.recalculateDosageSchedule(today, { })
                    }
                })
            }
        })
    }
}