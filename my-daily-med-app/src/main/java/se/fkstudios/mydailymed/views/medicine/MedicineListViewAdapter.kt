package se.fkstudios.mydailymed.views.medicine

import android.content.Context
import android.content.Intent
import android.databinding.DataBindingUtil
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import se.fkstudios.mydailymed.R
import se.fkstudios.mydailymed.databinding.ListItemMedicineBinding
import se.fkstudios.mydailymed.models.db.DbManager
import se.fkstudios.mydailymed.models.db.EditableDbFacade
import se.fkstudios.mydailymed.viewmodels.common.DelegateCommand
import se.fkstudios.mydailymed.viewmodels.medicine.MedicinesViewModel
import se.fkstudios.mydailymed.views.common.EXTRA_MEDICINE_ID
import se.fkstudios.mydailymed.views.common.NotifyAdapterOnListChangedCallback

class MedicineListViewAdapter(
        private val context: Context,
        private val viewModel: MedicinesViewModel,
        private val editableDbFacade: EditableDbFacade = DbManager.editableFacade)
    : BaseAdapter() {
    init {
        val notifyAdapterOnChangedCallback = NotifyAdapterOnListChangedCallback<MedicinesViewModel>(this)
        viewModel.medicines.addOnListChangedCallback(notifyAdapterOnChangedCallback)
    }

    override fun getView(position: Int, view: View?, parent: ViewGroup?): View {
        val binding: ListItemMedicineBinding = getOrCreateBinding(view, parent)
        val medicineListItem = viewModel.medicines[position]
        val medicine = medicineListItem.medicine;
        binding.viewModel = medicineListItem
        binding.editMedicineCommand = DelegateCommand({
            val intent = Intent(context, AddOrEditMedicineActivity::class.java)
            intent.putExtra(EXTRA_MEDICINE_ID, medicineListItem.id)
            context.startActivity(intent)
        })
        binding.removeMedicineCommand = DelegateCommand({
            AlertDialog.Builder(context)
                    .setTitle(context.getString(R.string.delete))
                    .setMessage("Are you sure you want to delete ${medicine.name}?")
                    .setPositiveButton(R.string.yes, { _, _ ->
                        editableDbFacade.removeMedicine(medicine, { viewModel.medicines.remove(medicineListItem) })
                        viewModel.update()
                    })
                    .setNegativeButton(R.string.no, null).show()

        })
        return binding.root
    }

    private fun getOrCreateBinding(view: View?, parent: ViewGroup?): ListItemMedicineBinding =
            if (view != null)
                DataBindingUtil.getBinding<ListItemMedicineBinding>(view)
            else
                DataBindingUtil.inflate<ListItemMedicineBinding>(LayoutInflater.from(context), R.layout.list_item_medicine, parent, false)

    override fun getItem(position: Int): Any = viewModel.medicines[position]

    override fun getItemId(position: Int): Long = viewModel.medicines[position].id.toLong()

    override fun getCount(): Int = viewModel.medicines.size
}