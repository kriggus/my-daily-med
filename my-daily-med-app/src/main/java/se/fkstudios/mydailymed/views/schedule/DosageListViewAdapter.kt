package se.fkstudios.mydailymed.views.schedule

import android.app.Activity
import android.databinding.DataBindingUtil
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.PopupMenu
import android.widget.Toast
import se.fkstudios.mydailymed.R
import se.fkstudios.mydailymed.databinding.ListItemDosageBinding
import se.fkstudios.mydailymed.models.*
import se.fkstudios.mydailymed.models.MedicineDosageMode.relativeDosages
import se.fkstudios.mydailymed.models.db.DbManager
import se.fkstudios.mydailymed.models.db.EditableDbFacade
import se.fkstudios.mydailymed.viewmodels.common.DelegateCommand
import se.fkstudios.mydailymed.viewmodels.schedule.DosageScheduleViewModel
import se.fkstudios.mydailymed.viewmodels.schedule.DosageViewModel
import se.fkstudios.mydailymed.views.common.NotifyAdapterOnListChangedCallback
import se.fkstudios.mydailymed.views.time.TimePickerFragment
import java.util.*


class DosageListViewAdapter(
        private val activity: Activity,
        private val viewModel: DosageScheduleViewModel,
        private val dbFacade: EditableDbFacade = DbManager.editableFacade)
    : BaseAdapter() {

    init {
        val notifyAdapterOnChangedCallback = NotifyAdapterOnListChangedCallback<DosageScheduleViewModel>(this)
        viewModel.dosageViewModels.addOnListChangedCallback(notifyAdapterOnChangedCallback)
    }

    override fun getView(position: Int, view: View?, parent: ViewGroup?): View {
        val binding: ListItemDosageBinding = getOrInflateBinding(view, parent)
        binding.viewModel = viewModel.dosageViewModels[position]

        binding.toggleIsDosedCommand = DelegateCommand({ toggleIsDosed(position) })

        binding.popupMenuCommand = DelegateCommand({
            val dosage = viewModel.dosageViewModels[position].dosage
            val popup = PopupMenu(activity, binding.root)
            popup.menuInflater.inflate(getMenuLayoutId(dosage), popup.getMenu())
            popup.setOnMenuItemClickListener { item ->
                when (item.itemId) {
                    R.id.menu_item_set_dosed_time -> showSetDosedTimeDialog(position)
                    R.id.menu_item_set_planned_time -> showSetPlannedTimeDialog(position)
                    R.id.menu_item_skip, R.id.menu_item_dont_skip -> {
                        toggleEnabled(position)
                    }
                }
                true
            }
            popup.show()
        })

        return binding.root
    }

    private fun getMenuLayoutId(dosage: Dosage): Int = when (dosage.isEnabled) {
        true -> R.menu.menu_dosage_list_item_popup_skip
        false -> R.menu.menu_dosage_list_item_popup_dont_skip
    }

    private fun getOrInflateBinding(view: View?, parent: ViewGroup?): ListItemDosageBinding = when (view) {
        null -> inflateBinding(parent)
        else -> getBinding(view)
    }

    private fun getBinding(view: View) = DataBindingUtil.getBinding<ListItemDosageBinding>(view)

    private fun inflateBinding(parent: ViewGroup?) =
            DataBindingUtil.inflate<ListItemDosageBinding>(LayoutInflater.from(activity), R.layout.list_item_dosage, parent, false)

    override fun getItem(position: Int): Any = viewModel.dosageViewModels[position]

    override fun getItemId(position: Int): Long = viewModel.dosageViewModels[position].id.toLong()

    override fun getCount(): Int = viewModel.dosageViewModels.size

    private fun toggleIsDosed(position: Int) {
        if (position !in viewModel.dosageViewModels.indices) return
        val dosageSchedule: DosageSchedule = viewModel.dosageSchedule ?: return

        val dosageViewModel = viewModel.dosageViewModels[position]
        val dosage: Dosage = dosageViewModel.dosage

        if (dosageViewModel.isDosed) {
            dosage.dosed = null
        } else {
            dosage.dosed = Date()
        }

        dbFacade.updateDosage(dosageSchedule, dosage, {
            viewModel.notifyDosagesIsDosedChanged()
        }, { error ->
            Toast.makeText(activity, error.message, Toast.LENGTH_LONG).show()
        })
    }

    private fun toggleEnabled(position: Int) {
        if (position !in viewModel.dosageViewModels.indices) return
        val dosageSchedule: DosageSchedule = viewModel.dosageSchedule ?: return

        val dosageViewModel = viewModel.dosageViewModels[position]
        val dosage: Dosage = dosageViewModel.dosage

        dosage.isEnabled = !dosage.isEnabled
        dosage.notifiedAsUpcoming = false
        dosage.notifiedAsMissed = false
        dbFacade.updateDosage(dosageSchedule, dosage, {
            viewModel.notifyDosagesEnabledChanged()
        }, { error ->
            Toast.makeText(activity, error.message, Toast.LENGTH_LONG).show()
        })
    }

    private fun showSetDosedTimeDialog(position: Int) {
        val dosage: Dosage = viewModel.dosageViewModels[position].dosage
        val presetDosed: Date = dosage.dosed ?: Date()
        val timePickerFragment = TimePickerFragment()
        timePickerFragment.hour = presetDosed.hourOfDay
        timePickerFragment.minute = presetDosed.minute
        timePickerFragment.listener = { hourOfDay: Int, minute: Int ->
            setDosedTime(position, hourOfDay, minute)
        }
        timePickerFragment.show(activity.fragmentManager, null)
    }

    private fun setDosedTime(position: Int, hourOfDay: Int, minute: Int) {
        if (position !in viewModel.dosageViewModels.indices) return
        val dosageSchedule: DosageSchedule = viewModel.dosageSchedule ?: return

        val dosageViewModel = viewModel.dosageViewModels[position]
        val dosage: Dosage = dosageViewModel.dosage

        dosage.dosed = dosageSchedule.day.addHoursOfDayToNewDate(hourOfDay).addMinutesToNewDate(minute)
        dosage.notifiedAsUpcoming = false
        dosage.notifiedAsMissed = false

        dbFacade.updateDosage(dosageSchedule, dosage, {
            viewModel.notifyDosagesIsDosedChanged()
        }, { error ->
            Toast.makeText(activity, error.message, Toast.LENGTH_LONG).show()
        })
    }

    private fun showSetPlannedTimeDialog(position: Int) {
        val dosage: Dosage = viewModel.dosageViewModels[position].dosage
        val timePickerFragment = TimePickerFragment()
        timePickerFragment.hour = dosage.planned.hourOfDay
        timePickerFragment.minute = dosage.planned.minute
        timePickerFragment.listener = { hourOfDay: Int, minute: Int ->
            val dependentDosages: List<DosageViewModel> = viewModel.dosageViewModels.filter { isFirstDependentOnSecond(it.dosage, dosage) }
            if (dependentDosages.isNotEmpty()) {
                showMoveDependentDosagesDialog(dosage.medicine.name, dosage.planned.hourOfDay, dosage.planned.minute, hourOfDay, minute, dependentDosages)
            }
            setPlannedTime(position, hourOfDay, minute)
        }
        timePickerFragment.show(activity.fragmentManager, null)
    }

    private fun showMoveDependentDosagesDialog(medicineName: String, oldHourOfDay: Int, oldMinute: Int, newHourOfDay: Int, newMinute: Int, dependentDosageViewModels: List<DosageViewModel>) {
        val diffHour = newHourOfDay - oldHourOfDay
        val unsignedDiffHour: Int = Math.abs(diffHour)
        val diffMinute = newMinute - oldMinute
        val unsignedDiffMinute: Int = Math.abs(diffMinute)
        AlertDialog.Builder(activity)
                .setTitle(R.string.move_dependent_dosages)
                .setMessage("There are ${dependentDosageViewModels.size} dosages depending on changed $medicineName. Move them ${String.format("%d:%02d", unsignedDiffHour, unsignedDiffMinute)} as well?")
                .setPositiveButton(R.string.yes, { _, _ ->
                    val dependentDosages = dependentDosageViewModels.map { it.dosage }
                    movePlannedTimes(dependentDosages, diffHour, diffMinute)

                })
                .setNegativeButton(R.string.no, null).show()
    }

    private fun movePlannedTimes(dosages: List<Dosage>, diffHour: Int, diffMinute: Int) {
        val dosageSchedule: DosageSchedule = viewModel.dosageSchedule ?: return

        dosages.forEach { dosage ->
            val hourOfDay: Int = dosage.planned.hourOfDay + diffHour
            val minute: Int = dosage.planned.minute + diffMinute
            dosage.planned = dosage.planned.addHoursOfDayToNewDate(hourOfDay).addMinutesToNewDate(minute)
            dosage.notifiedAsUpcoming = false
            dosage.notifiedAsMissed = false
        }

        dbFacade.updateDosages(dosageSchedule, dosages, {
            viewModel.dosageViewModels.sortBy { it.dosage.planned }
            viewModel.notifyDosagesPlannedChanged()
        }, { error ->
            Toast.makeText(activity, error.message, Toast.LENGTH_LONG).show()
        })
    }

    private fun setPlannedTime(position: Int, hourOfDay: Int, minute: Int) {
        if (position !in viewModel.dosageViewModels.indices) return
        val dosageSchedule: DosageSchedule = viewModel.dosageSchedule ?: return

        val dosageViewModel = viewModel.dosageViewModels[position]
        val dosage: Dosage = dosageViewModel.dosage

        dosage.planned = dosageSchedule.day.addHoursOfDayToNewDate(hourOfDay).addMinutesToNewDate(minute)
        dosage.notifiedAsUpcoming = false
        dosage.notifiedAsMissed = false

        dbFacade.updateDosage(dosageSchedule, dosage, {
            viewModel.dosageViewModels.sortBy { it.dosage.planned }
            viewModel.notifyDosagesPlannedChanged()
        }, { error ->
            Toast.makeText(activity, error.message, Toast.LENGTH_LONG).show()
        })
    }

    private fun isFirstDependentOnSecond(first: Dosage, second: Dosage) = first.planned > second.planned
            && first != second
            && first.medicine == second.medicine
            && first.medicine.dosageMode == relativeDosages
}