package se.fkstudios.mydailymed.views.common

import android.databinding.BindingConversion
import android.view.View

@BindingConversion fun convertBooleanToVisibility(visible: Boolean): Int = if (visible) View.VISIBLE else View.GONE

