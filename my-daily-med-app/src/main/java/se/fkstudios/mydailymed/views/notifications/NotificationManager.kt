package se.fkstudios.mydailymed.views.notifications

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.TaskStackBuilder
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.os.Build
import android.support.annotation.RequiresApi
import android.support.v4.app.NotificationCompat
import se.fkstudios.mydailymed.R
import se.fkstudios.mydailymed.models.Dosage
import se.fkstudios.mydailymed.models.DosageSchedule
import se.fkstudios.mydailymed.models.db.DbManager
import se.fkstudios.mydailymed.models.db.EditableDbFacade
import se.fkstudios.mydailymed.views.main.MainActivity
import se.fkstudios.mydailymed.views.toCommaSeparatedDisplayString
import se.fkstudios.mydailymed.views.unique
import java.util.*


class NotificationManager(
        private val context: Context,
        private val editableDbFacade: EditableDbFacade = DbManager.editableFacade
) {

    init {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createNotificationChannel()
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel() {
        val notificationChannel = NotificationChannel(NOTIFICATIONS_CHANNEL_ID, NOTIFICATIONS_CHANNEL_NAME, NotificationManager.IMPORTANCE_DEFAULT)
        getNotificationManager()?.createNotificationChannel(notificationChannel)
    }

    fun notifyDosagesIn5Minutes(schedule: DosageSchedule, dosages: List<Dosage>) {
        val text: String = dosages
                .map { it.medicine.name }
                .unique()
                .toCommaSeparatedDisplayString()

        notify(MainActivity::class.java,
                NOTIFICATION_ID_DOSAGES_IN_5_MIN,
                context.getString(R.string.soon_to_dose_notification_title),
                context.getString(R.string.soon_to_dose_notification_text, text)
        )

        dosages.forEach {
            it.notifiedAsUpcoming = true
            editableDbFacade.updateDosage(schedule, it)
        }
    }

    fun notifyDosagesMissed(schedule: DosageSchedule, dosages: List<Dosage>, whenValue: Long) {
        val text: String = dosages
                .map { it.medicine.name }
                .unique()
                .toCommaSeparatedDisplayString()

        notify(MainActivity::class.java,
                NOTIFICATION_ID_DOSAGES_MISSED,
                context.getString(R.string.missed_to_dose_notification_title),
                context.getString(R.string.missed_to_dose_notification_text, text),
                whenValue
        )

        dosages.forEach {
            it.notifiedAsMissed = true
            editableDbFacade.updateDosage(schedule, it)
        }
    }

    private fun notify(onClickActivityClass: Class<*>, id: Int, title: String, text: String, whenValue: Long = Date().time) {
        val builder = NotificationCompat.Builder(context, NOTIFICATIONS_CHANNEL_ID)
        builder.setContentTitle(title)
        builder.setContentText(text)
        builder.setSmallIcon(R.drawable.ic_notification_dark_24dp)
        builder.setStyle(NotificationCompat.BigTextStyle().bigText(text))
        builder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
        builder.setVibrate(longArrayOf(500, 1000))
        builder.setOnlyAlertOnce(true)
        builder.setAutoCancel(true)
        builder.setShowWhen(true)
        builder.setWhen(whenValue)
        builder.priority = 2

        val contentIntent = Intent(context, onClickActivityClass)
        contentIntent.putExtra(EXTRA_NOTIFICATION_ID, id)

        val stackBuilder = TaskStackBuilder.create(context)
        stackBuilder.addNextIntent(contentIntent)
        stackBuilder.addParentStack(onClickActivityClass)

        val pendingContentIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT)
        builder.setContentIntent(pendingContentIntent)

        getNotificationManager()?.notify(id, builder.build())
    }

    private fun getNotificationManager(): NotificationManager? = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager?
}