package se.fkstudios.mydailymed.views.main

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import se.fkstudios.mydailymed.R
import se.fkstudios.mydailymed.views.common.SelectableFragment

class MainActivity : AppCompatActivity() {

    var mainFragmentPageAdapter: MainFragmentPageAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initTabs()
    }

    private fun initTabs() {
        mainFragmentPageAdapter = MainFragmentPageAdapter(supportFragmentManager)
        viewPagerMain.adapter = mainFragmentPageAdapter
        viewPagerMain.offscreenPageLimit = viewPagerMain.adapter?.count ?: 1 - 1
        viewPagerMain.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabLayoutMain))
        tabLayoutMain.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {

            override fun onTabReselected(tab: TabLayout.Tab?) = Unit

            override fun onTabUnselected(tab: TabLayout.Tab?) {
                val position: Int = tab?.position ?: 0
                viewPagerMain.currentItem = position
                val fragment: Fragment = findFragmentByPosition(position) ?: return
                (fragment as? SelectableFragment)?.onUnselected()
            }

            override fun onTabSelected(tab: TabLayout.Tab?) {
                val position: Int = tab?.position ?: 0
                viewPagerMain.currentItem = position
                val fragment: Fragment = findFragmentByPosition(position) ?: return
                (fragment as? SelectableFragment)?.onSelected()
            }
        })
    }

    private fun findFragmentByPosition(position: Int): Fragment? {
        val tag = "android:switcher:${viewPagerMain.id}:$position"
        return supportFragmentManager.findFragmentByTag(tag)
    }
}
