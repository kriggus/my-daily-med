package se.fkstudios.mydailymed.views.medicine

import android.databinding.DataBindingUtil
import android.databinding.Observable
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_add_or_edit_medicine.*
import se.fkstudios.mydailymed.BR
import se.fkstudios.mydailymed.R
import se.fkstudios.mydailymed.databinding.ActivityAddOrEditMedicineBinding
import se.fkstudios.mydailymed.models.MedicineDosageMode
import se.fkstudios.mydailymed.models.db.DbManager
import se.fkstudios.mydailymed.viewmodels.common.DelegateCommand
import se.fkstudios.mydailymed.viewmodels.medicine.MedicineTemplateToModelMapper
import se.fkstudios.mydailymed.viewmodels.medicine.MedicineTemplateViewModel
import se.fkstudios.mydailymed.views.common.EXTRA_MEDICINE_ID
import se.fkstudios.mydailymed.views.common.ResourceStringProvider
import se.fkstudios.mydailymed.views.doDelayed

class AddOrEditMedicineActivity : AppCompatActivity() {

    private var viewModel: MedicineTemplateViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val medicineId = intent.getIntExtra(EXTRA_MEDICINE_ID, -1)
        val viewModel: MedicineTemplateViewModel = when (medicineId) {
            -1 -> MedicineTemplateViewModel(ResourceStringProvider(this))
            else -> MedicineTemplateViewModel(medicineId, ResourceStringProvider(this))
        }
        this.viewModel = viewModel

        val binding = DataBindingUtil.setContentView<ActivityAddOrEditMedicineBinding>(this, R.layout.activity_add_or_edit_medicine)
        binding.viewModel = viewModel
        binding.addMedicineTimeCommand = DelegateCommand({ viewModel.addTimeTemplate() })

        initDosageModeSpinner(viewModel)
        initMedicineTimesListView(viewModel)
        initToolbar()
    }

    private fun initDosageModeSpinner(viewModel: MedicineTemplateViewModel) {
        val dosageModeAdapter = ArrayAdapter.createFromResource(this, R.array.medicine_dosage_mode, R.layout.spinner_item_dosage_mode)
        dosageModeAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item_dosage_mode)
        spinnerDosageType.adapter = dosageModeAdapter
        spinnerDosageType.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) = Unit
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                viewModel.dosageMode = viewIndexToDosageMode(position)
            }
        }
        spinnerDosageType.setSelection(dosageModeToViewIndex(viewModel.dosageMode))
        viewModel.addOnPropertyChangedCallback(object : Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
                if (propertyId == BR.dosageMode || propertyId == BR._all) {
                    val viewIndex = dosageModeToViewIndex(viewModel.dosageMode)
                    if (viewIndex != spinnerDosageType.selectedItemPosition) {
                        spinnerDosageType.setSelection(viewIndex)
                    }
                }
            }
        })
    }

    private fun initMedicineTimesListView(viewModel: MedicineTemplateViewModel) {
        val medicineTimesAdapter = MedicineTimeListViewAdapter(this, viewModel)
        listViewMedicineTimes.adapter = medicineTimesAdapter
        viewModel.addOnPropertyChangedCallback(object : Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(self: Observable?, propertyId: Int) {
                if (propertyId == BR.timeTemplates || propertyId == BR._all) {
                    medicineTimesAdapter.notifyDataSetChanged()
                }
            }
        })

        val footerView: View = layoutInflater.inflate(R.layout.layout_empty_footer, listViewMedicineTimes, false)
        listViewMedicineTimes.addFooterView(footerView, null, false)
        listViewMedicineTimes.dividerHeight = 0
    }

    private fun initToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onResume() {
        super.onResume()
        doDelayed(333) {
            floatingActionButton.show()
        }
    }

    override fun onPause() {
        floatingActionButton.hide()
        super.onPause()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_medicine, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            R.id.menu_item_save -> {
                viewModel?.let { saveChangesInViewModel(it) }
                true
            }
            else -> false
        }
    }

    private fun saveChangesInViewModel(viewModel: MedicineTemplateViewModel) {
        if (viewModel.isValid) {
            val toModelMapper = MedicineTemplateToModelMapper()
            val medicine = toModelMapper.toModel(viewModel)
            DbManager.editableFacade.addOrUpdateMedicine(medicine, {
                finish()
            }, { error ->
                Toast.makeText(this, error.message, Toast.LENGTH_LONG).show()
            })
        } else {
            Toast.makeText(this, getString(R.string.cannot_save_medicine), Toast.LENGTH_SHORT).show()
        }
    }

    private fun viewIndexToDosageMode(index: Int): MedicineDosageMode = when (index) {
        0 -> MedicineDosageMode.relativeDosages
        1 -> MedicineDosageMode.specificDosages
        else -> throw IndexOutOfBoundsException("Unsupported view index as dosage mode")
    }

    private fun dosageModeToViewIndex(mode: MedicineDosageMode): Int = when (mode) {
        MedicineDosageMode.specificDosages -> 1
        MedicineDosageMode.relativeDosages -> 0
    }
}