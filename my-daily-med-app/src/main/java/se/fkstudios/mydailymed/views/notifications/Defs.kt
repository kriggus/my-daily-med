package se.fkstudios.mydailymed.views.notifications

const val EXTRA_NOTIFICATION_ID = "se.fkstudios.mydailymed.views.notifications.EXTRA_NOTIFICATION_ID"
const val NOTIFICATION_ID_DOSAGES_IN_5_MIN = 1
const val NOTIFICATION_ID_DOSAGES_MISSED = 2
const val NOTIFICATION_ID_FOREGROUND_SERVICES = 3
const val NOTIFICATIONS_CHANNEL_ID = "se.fkstudios.mydailymed.views.notifications.NOTIFICATIONS_CHANNEL_ID"
const val NOTIFICATIONS_CHANNEL_NAME = "My Daily Med Notifications Channel"