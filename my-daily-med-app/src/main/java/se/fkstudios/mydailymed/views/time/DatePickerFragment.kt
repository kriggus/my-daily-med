package se.fkstudios.mydailymed.views.time

import android.app.DatePickerDialog
import android.app.Dialog
import android.app.DialogFragment
import android.os.Bundle
import android.widget.DatePicker
import se.fkstudios.mydailymed.R
import se.fkstudios.mydailymed.models.currentCalendarYear
import se.fkstudios.mydailymed.models.currentDayOfMonth
import se.fkstudios.mydailymed.models.currentMonthOfYear

class DatePickerFragment : DialogFragment(), DatePickerDialog.OnDateSetListener {

    var listener: ((year: Int, month: Int, dayOfMonth: Int) -> Unit)? = null
    var year: Int? = null
    var month: Int? = null
    var dayOfMonth: Int? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog = DatePickerDialog(activity,
            R.style.DateAndTimeDialogTheme,
            this,
            year ?: currentCalendarYear(),
            month ?: currentMonthOfYear(),
            dayOfMonth ?: currentDayOfMonth())

    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        listener?.invoke(year, month, dayOfMonth)
    }
}
