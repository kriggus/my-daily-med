package se.fkstudios.mydailymed.views.common

import android.content.Context
import android.graphics.drawable.Drawable
import se.fkstudios.mydailymed.viewmodels.common.DrawableProvider

class ResourceDrawableProvider(private val context: Context) : DrawableProvider {
    override fun get(id: Int): Drawable = context.getDrawable(id)
}